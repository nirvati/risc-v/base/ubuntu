# Ubuntu in Docker

This is a Docker container for Ubuntu, basically the same as the official one, but for amd64, arm64 and RISC-V.

Builds are provided for Ubuntu 22.04 and 23.04 and are updated weekly.
